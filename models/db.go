package models

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/lib/pq"
	log "github.com/sirupsen/logrus"

	"todo-backend/settings"
)

// MigrateDatabase do database migration
func MigrateDatabase(client *gorm.DB) {
	client.AutoMigrate(&User{})
	client.AutoMigrate(&Todo{})
}

// GetDBClient function for connecting the database
func GetDBClient() *gorm.DB {
	connectionString, err := pq.ParseURL(settings.Settings.DbURL)
	if err != nil {
		log.Error(err)
		panic(err)
	}

	database, err := gorm.Open("postgres", connectionString)
	if err != nil {
		log.Error(err)
		panic(err)
	}

	database.DB().SetMaxIdleConns(settings.Settings.IdleConnections)
	database.DB().SetMaxOpenConns(settings.Settings.MaxOpenConnections)
	database.DB().SetConnMaxLifetime(settings.Settings.MaxLifetime)
	database.LogMode(settings.Settings.Debug)
	return database
}
