package models

import "time"

type User struct {
	ID       int32      `json:"-" gorm:"primaryKey;autoIncrement"`
	Sequence int32      `json:"-" gorm:"not null;index"`
	TodoID   int32      `json:"-" gorm:"not null;index"`
	Username string     `json:"username" validate:"required" gorm:"not null;index"`
	Created  *time.Time `json:"created,omitempty" gorm:"not null;autoCreateTime;DEFAULT:current_timestamp"`
}
