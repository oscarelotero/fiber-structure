package models

import "time"

type Todo struct {
	ID      int32     `json:"id" gorm:"primaryKey;autoIncrement"`
	Name    string    `json:"name" validate:"required" gorm:"not null;index"`
	Created time.Time `json:"created" gorm:"not null;autoCreateTime;DEFAULT:current_timestamp"`
	User    []User    `json:"user"`
}
