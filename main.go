package main

import (
	"flag"
	"os"

	"github.com/gofiber/cors"
	"github.com/gofiber/fiber"
	"github.com/gofiber/fiber/middleware"
	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"

	"todo-backend/api"
	"todo-backend/models"
	"todo-backend/settings"
	"todo-backend/utils"
)

func loadEnv() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Info("Environment vars load from OS")
	} else {
		log.Info("Environment vars load from .env file ")
	}
}

func main() {
	var migrate bool
	flag.BoolVar(&migrate, "migrate", false, "apply migrations")
	flag.Parse()

	loadEnv()
	settings.LoadSettings(&settings.Settings)
	dbClient := models.GetDBClient()
	defer dbClient.Close()

	if migrate {
		log.Info("Migrate database")
		models.MigrateDatabase(dbClient)
		os.Exit(0)
	}

	app := fiber.New()
	utils.SetErrorHandler(app)
	app.Use(middleware.Recover())

	// TODO: Add the specific Origin for cors
	app.Use(cors.New())

	api.SetupHealthCheck(app)
	api.SetupTodoRoutes(app, dbClient)
	api.SetupUserRoutes(app, dbClient)

	log.Info("starting...")
	port := os.Getenv("PORT")
	if port == "" {
		port = "3001"

	}
	log.Printf("Listening on port %s\n\n", port)
	log.Fatal(app.Listen(port))
}
