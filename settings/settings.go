package settings

import (
	"os"
	"strconv"
	"time"
)

type settings struct {
	Debug              bool
	FirebaseProject    string
	FirebaseConfig     string
	DbURL              string
	IdleConnections    int
	MaxOpenConnections int
	MaxLifetime        time.Duration
}

// Settings config/setting var
var Settings settings

// LoadSettings Function for load settings
func LoadSettings(s *settings) {
	s.Debug, _ = strconv.ParseBool(os.Getenv("DEBUG"))
	s.FirebaseProject = os.Getenv("FIREBASE_PROJECT")
	s.FirebaseConfig = os.Getenv("FIREBASE_CONFIG")
	s.DbURL = os.Getenv("DATABASE_URL")
	s.IdleConnections, _ = strconv.Atoi(os.Getenv("DATABASE_IDLE_CONNECTIONS"))
	s.MaxOpenConnections, _ = strconv.Atoi(os.Getenv("DATABASE_MAX_OPEN_CONNECTIONS"))
	maxTime, _ := strconv.Atoi(os.Getenv("DATABASE_MAX_LIFETIME_MINUTES"))
	s.MaxLifetime = time.Duration(maxTime) * time.Minute
}
