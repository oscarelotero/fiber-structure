package api

import (
	"strconv"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber"
	log "github.com/sirupsen/logrus"

	"todo-backend/models"
)

// Create the User
// POST /api/todo/:id/user/
func (w *WebServices) AddUserToList(c *fiber.Ctx) {
	var json models.User

	if err := c.BodyParser(&json); err != nil {
		log.Error(err)
		err := fiber.NewError(400, "invalid data")
		c.Next(err)
		return
	}

	validate := validator.New()
	err := validate.Struct(json)
	if err != nil {
		log.Error(err)
		err := fiber.NewError(400, err.Error())
		c.Next(err)
		return
	}

	// Get url param todo id
	i, err := strconv.ParseInt(c.Params("todo"), 10, 32)
	if err != nil {
		log.Error(err)
		err := fiber.NewError(400, "invalid todo value")
		c.Next(err)
		return
	}
	todoID := int32(i)

	// Check if todo exist
	err = w.todo.GetTodo(todoID, &models.Todo{})
	if err != nil {
		log.Error(err)
		err := fiber.NewError(404, "todo does not exist")
		c.Next(err)
		return
	}

	err = w.user.SaveUser(todoID, &json)
	if err != nil {
		log.Error(err)
		err := fiber.NewError(400, err.Error())
		c.Next(err)
		return
	}

	_ = c.JSON(json)
}

// Create the user
// POST /api/todo/:id/validate-user/
func (w *WebServices) GetUser(c *fiber.Ctx) {
	var json models.User
	if err := c.BodyParser(&json); err != nil {
		log.Error(err)
		err := fiber.NewError(400, "invalid data")
		c.Next(err)
		return
	}

	validate := validator.New()
	err := validate.Struct(json)
	if err != nil {
		log.Error(err)
		err := fiber.NewError(400, err.Error())
		c.Next(err)
		return
	}

	// Get url param todo id
	i, err := strconv.ParseInt(c.Params("todo"), 10, 32)
	if err != nil {
		log.Error(err)
		err := fiber.NewError(400, "invalid todo value")
		c.Next(err)
		return
	}
	todoID := int32(i)

	// Check if todo exist
	err = w.todo.GetTodo(todoID, &models.Todo{})
	if err != nil {
		log.Error(err)
		err := fiber.NewError(404, "todo does not exist")
		c.Next(err)
		return
	}

	err = w.user.GetUserFromTodo(todoID, &json)
	if err != nil {
		log.Error(err)
		err := fiber.NewError(404, err.Error())
		c.Next(err)
		return
	}

	_ = c.JSON(json)
}
