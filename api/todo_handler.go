package api

import (
	"strconv"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber"
	log "github.com/sirupsen/logrus"

	"todo-backend/models"
)

// Create the todo
// POST /api/todo/
func (w *WebServices) CreateTodoHandler(c *fiber.Ctx) {
	var json models.Todo

	if err := c.BodyParser(&json); err != nil {
		log.Error(err)
		err := fiber.NewError(400, "invalid data")
		c.Next(err)
		return
	}

	validate := validator.New()
	err := validate.Struct(json)
	if err != nil {
		log.Error(err)
		err := fiber.NewError(400, err.Error())
		c.Next(err)
		return
	}

	err = w.todo.CreateTodo(&json)
	if err != nil {
		log.Error(err)
		err := fiber.NewError(500, "error creating the todo")
		c.Next(err)
		return
	}

	_ = c.JSON(json)
}

// Get todo data
// GET /api/todo/:todo/
func (w *WebServices) GetTodoDetail(c *fiber.Ctx) {
	var json models.Todo

	// Get url param todo id
	i, err := strconv.ParseInt(c.Params("todo"), 10, 32)
	if err != nil {
		log.Error(err)
		err := fiber.NewError(400, "invalid todo value")
		c.Next(err)
		return
	}
	todoID := int32(i)

	err = w.todo.GetTodo(todoID, &json)
	if err != nil {
		c.SendStatus(404)
		return
	}

	_ = c.JSON(json)
}
