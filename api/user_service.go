package api

import (
	"errors"
	"fmt"

	"github.com/jinzhu/gorm"

	"todo-backend/models"
)

type UserInterface interface {
	SaveUser(todoID int32, ObjUser *models.User) error
	GetUserFromTodo(todoID int32, ObjUser *models.User) error
}

type UserService struct {
	*gorm.DB
}

func (s *UserService) SaveUser(todoID int32, objUser *models.User) error {
	var lastUser models.User

	err := s.Where("todo_id =? and username=?",
		todoID, objUser.Username).Take(&models.User{}).Error
	if err == nil {
		return fmt.Errorf("the username %s already exist", objUser.Username)
	}

	tx := s.Begin()
	objUser.TodoID = todoID

	err = tx.Where("todo_id =?", todoID).Last(&lastUser).Error
	if err != nil {
		// If the error is not found create the Sequence = 1
		if errors.Is(err, gorm.ErrRecordNotFound) {
			objUser.Sequence = 1
			err = tx.Create(objUser).Error
			if err != nil {
				tx.Rollback()
				return fmt.Errorf("todo not found")
			}
			return tx.Commit().Error
		} else {
			tx.Rollback()
			return fmt.Errorf("error saving the user")
		}
	}

	// Increment the Sequence and save
	objUser.Sequence = lastUser.Sequence + 1
	err = tx.Create(objUser).Error
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("error saving the user")
	}
	return tx.Commit().Error
}

func (s *UserService) GetUserFromTodo(todoID int32, objUser *models.User) error {
	err := s.Where("todo_id=? and username=?", todoID, objUser.Username).Take(objUser).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return fmt.Errorf("the username %s does not exist", objUser.Username)
		} else {
			return err
		}
	}
	return nil
}
