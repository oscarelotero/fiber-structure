package api

import (
	"github.com/gofiber/fiber"
	"github.com/jinzhu/gorm"
)

func SetupTodoRoutes(app *fiber.App, client *gorm.DB) {
	s := start(client)
	api := app.Group("/api")
	grp := api.Group("/todo")
	grp.Post("/", s.CreateTodoHandler)
	grp.Get("/:todo/", s.GetTodoDetail)
}

func SetupUserRoutes(app *fiber.App, client *gorm.DB) {
	s := start(client)
	api := app.Group("/api")
	grp := api.Group("/todo")
	grp.Post("/:todo/user/", s.AddUserToList)
	grp.Post("/:todo/validate-user/", s.GetUser)
}

func SetupHealthCheck(app *fiber.App) {
	app.Get("/api/healthz/", func(c *fiber.Ctx) {
		_ = c.JSON(fiber.Map{
			"status": "UP",
		})
	})
}
