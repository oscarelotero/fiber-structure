package api

import "github.com/jinzhu/gorm"

type Services struct {
	todo TodoInterface
	user UserInterface
}

func NewServices(client *gorm.DB) Services {
	return Services{
		todo: &TodoService{client},
		user: &UserService{client},
	}
}

type WebServices struct {
	Services
}

func start(client *gorm.DB) *WebServices {
	return &WebServices{NewServices(client)}
}
