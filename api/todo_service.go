package api

import (
	"github.com/jinzhu/gorm"
	log "github.com/sirupsen/logrus"

	"todo-backend/models"
)

type TodoInterface interface {
	CreateTodo(objTodo *models.Todo) error
	GetTodo(streamID int32, objTodo *models.Todo) error
}

type TodoService struct {
	*gorm.DB
}

func (s *TodoService) CreateTodo(objTodo *models.Todo) error {
	err := s.Create(objTodo).Error
	if err != nil {
		log.Error("cannot save todo " + err.Error())
		return err
	}
	return nil
}

func (s *TodoService) GetTodo(streamID int32, objTodo *models.Todo) error {
	err := s.Preload("User").First(objTodo, streamID).Error
	if err != nil {
		log.Errorf("cannot get todo %d %s", streamID, err.Error())
		return err
	}
	return nil
}
