module todo-backend

go 1.14

require (
	github.com/go-playground/validator/v10 v10.2.0
	github.com/gofiber/cors v0.2.2
	github.com/gofiber/fiber v1.14.6
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/lib/pq v1.1.1
	github.com/sirupsen/logrus v1.6.0
	golang.org/x/sys v0.0.0-20201101102859-da207088b7d1 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
